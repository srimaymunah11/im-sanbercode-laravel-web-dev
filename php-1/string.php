
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        
        $first_sentence = "Hello PHP!";
        echo "Kalimat Pertama : " . $first_sentence . "<br>";
        echo "Panjang Kalimat 1 : " .strlen($first_sentence) . "<br>" ;
        echo "Jumlah Kata Kalimat 1 : " . str_word_count($first_sentence) . "<br>" ;

        $second_sentence = "I'm ready for the challenges";
        echo "Kalimat Kedua : " . $second_sentence . "<br>";
        echo "Panjang Kalimat 2 : " .strlen($second_sentence) . "<br>" ;
        echo "Jumlah Kata Kalimat 2 : " . str_word_count($second_sentence) . "<br>" ;


        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        echo "Kata kedua: " . substr($string2, 2, 4) . "<br>" ;
        echo "Kata Ketiga: " . substr($string2, 7, 3) . "<br>" ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.

        */
        $string3 = "PHP is old but sexy!";
        echo "Kalimat : " . $string3 . "<br>";
        echo "Ganti String Kalimat : " . str_replace("sexy","awesome", $string3);
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>